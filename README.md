## Malayalam SpellChecker LibreOffice Extension v.2.4
--------------------------------------------------

## Developed Jointly By:
CLRG,AU-KBC Chennai and ICFOSS Trivandrum

Spell Checker to validate Malayalam words in Libreoffice Writer. Presently the Error words are highlighted in RED font and suggestiion words are given following the error words in brackets in RED font. Normalisation of Unicode fonts such Chillu letter and letters formed with zero joiner are also corrected and presented as suggestion word.

## Requirement:
python 3

## How to Install:
Step 1:
Copy the resource files to the home directory:
The resource files are provided in 'splrsc' folder.

## Commands
mkdir ~/.splrsc
cp splrsc/* ~/.splrsc/.

Step 2:
a, Open LibreOffice (> 4.2)
b, Click 'Tool' in the Tool bar
c, In the Drop-down menu click 'Extension Manager', a command box will open
d, Click on the Add button and select OXT zip file (SpellCheck2.4.zip). Wait till it appears in the command box. e, Close the Command Window and Restart LibreOffice.
f, You will see a new button 'SC' in the Tool Bar

Contents:
├── ml_words.txt - Malayalam word list used in the development of the spell checker
├── SpellCheck2.4.zip - Malayalam Spell checker OXT
└── splrsc
    ├── 3gm_freq.txt - character frequency dctionary
    ├── 3gm.txt - Possible characters dictionary
    ├── charIndex.txt - Malayalam character index
    ├── charRsc.txt - list of possible characters
    ├── f_char.txt - list of first charcters
    ├── f_dfsa.txt - Forward Deterministic Finite State Automata
    ├── l_char.txt - list of last characters
    └── r_dfsa.txt - Reverse Deterministic Finite State Automata


## SpellChecking the Text:

a) Once the text is typed, click on 'SC' button in the tool bar and a drop-down menu 'SpellCheck' will appear.
Click 'SpellCheck' button. The text will be spell checked and Error words will be presented in RED font. Once the Spell check is completed a pop-up box will appear with 'Spell check completed' message. 

b) Select the Error Word, which is in RED font, and click on 'SC' button in the tool bar and click 'SpellCheck' in the Drop-down menu, a command box with two radio buttons 'Add' and 'Replace' and a list of suggestion words 

If we need to add the error word in the dictionary, click 'Add to Dictionary' radio button and press submit.

If we need to replace the error word with the suggestion words, we need to click the 'Replace' radio button and  select the correct suggestion and then press 'Submit' buttom, the suggestion word will be replaced.

c) If the error word is an agglutinated word then in the suggestion box instead of the suggestion words it will be mentioned as 'Agglutinated word'.


